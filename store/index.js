import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      posts: [
        {
          date: '05/ago',
          title: 'Escrita da proposta PR',
          slug: 'proposta-pr',
          content: `
Hoje eu escrevi a primeira versão da PR.
<br>
Como a minha atividade
está relacionada às atividades de mais membros do grupo,
tivemos uma reunião para deixarmos claro as atribuições de cada
um.
          `
        },
        {
          date: '17/ago',
          title: 'Definição do esqueleto do HackMOOC',
          slug: 'esqueleto-hack-mooc',
          content: `
          Nesta semana, tivemos reunião para a definição da estrutura do HackMOOC e uma renião com o Paulo para definirmos mais detalhes das trilhas de MOOCs.
          <br>
          A reunião do esqueleto do HackMOOC teve início com um <em>brainstorm</em> sobre tópicos que queremos abordar no curso -- isso foi uma primeira proposta de conteúdo. Disso, começamos a definir um pouco da estrutura de alto nível do curso: quais serão as seções e as respectivas subseções.
          <br>
          Além disso, determinamos que o curso terá momentos para o desenvolvimento de um projeto prático pelos alunos, porém ainda não fechamos o projeto em si. Temos algumas possibilidades:
          <ul>
            <li>Calculadora - um site simples que tem o formato de uma calculadora convencional e que de fato faz contas</li>
            <li>Agenda Escolar - uma plataforma para organização das tarefas escolares ou acadêmicas.</li>
          </ul>
          Temos também uma reunião com o Paulo Almeida, responsável pelos MOOCs no Coursera/USP para que ele possa acompanhar esse desenvolvimento inicial do curso. Nesta reunião, conversamos sobre a possibilidade de expandir os planos para que seja possível a emissão de vários certificados pelo Coursera. Com isso, a estrutura do curso ficou a seguinte:
          <ul>
            <li>Introdução à programação com JavaScript</li>
            <li>DevOps - Infraestrutura como código</li>
            <li><em>Front-end</em> (trilha) 
              <ul>
                <li>HackMOOC</li>
                <li>MPA com Vue.js</li>
                <li>SPA com Vue.js</li>
                <li>PWA com vue.js</li>
                <li>Nativo com NativeScript e Vue.js</li>
              </ul>
            </li>
            <li><em>Back-end</em> (trilha)
              <ul>
                <li>HackMOOC</li>
                <li>REST API com Node.js</li>
                <li>GraphQL API com Node.js</li>
                <li>Banco de dados SQL</li>
                <li>Banco de dados NoSQL</li>
              </ul>
            </li>
          </ul>
          Além disso, há a trilha que engloba todos esses cursos, chamada de <em>Full-Stack</em>.
          `
        },
        {
          date: '25/ago',
          title: 'HackMOOC e Hackathon Mobilidade',
          slug: 'aprofundamento-hack-mooc-hack-mobilidade',
          content: `
Nesta semana, aprofundamos o esqueleto do HackMOOC e demos os primeiros passos da organização do Hackathons de Mobilidade Ativa.
<br>
O esqueleto do HackMOOC havia sido definido em linhas gerais e numa escala grande. Durante esta semana, aprofundamos a descrição para termos o tema de cada seção do curso e, dentro de cada uma, iniciamos a definição dos tópicos de cada vídeo. Até agora, a estrutura está da seguinte forma:
<ol>
  <li>
    Introdução ao Curso
    <ol>
      <li>História</li>
      <li>Arquitetura</li>
    </ol>
  </li>
  <li>
    Setup
    <ol>
      <li>Linux</li>
      <li>Windows</li>
      <li>Mac</li>
    </ol>
  </li>
  <li>
    HTML
    <ol>
      <li>Introdução</li>
      <li>Fazendo marcações</li>
      <li>Interpretação do Documento (DOM)</li>
      <li>Agrupamento e Semântica</li>
    </ol>
  </li>
  <li>
    CSS básico
    <ol>
      <li>Introdução</li>
      <li>DOM e Cascateamento</li>
      <li>Box Model</li>
      <li>Classes e B.E.M.</li>
    </ol>
  </li>
  <li>
    CSS com Flexbox
  </li>
  <li>
    CSS com Grid
  </li>
  <li>
    Responsividade
    <ol>
      <li>Introdução (mobile-first)</li>
      <li><pre>@media</pre></li>
    </ol>
  </li>
  <li>
    Versionamento e Entrega
    <ol>
      <li>Git</li>
      <li>GitLab / GitHub</li>
      <li>Deploy</li>
    </ol>
  </li>
  <li>
    Projeto pt. 1
  </li>
  <li>
    JavaScript pt. 1
  </li>
  <li>
    JavaScript pt. 2
  </li>
  <li>
    Projeto pt. 2
  </li>
</ol>

Além disso, surgiu no meio da semana passada a possibilidade de organizarmos um Hackday em conjunto com uma ONG chamada Ciclocidade. A ideia é fazer protótipos de projetos que analisem dados de mobilidade urbana para buscar visualizações e soluções para a mobilidade ativa de mulheres. Foi decidido que o evento será exclusivo para o público feminino, como iniciativa de incentivo à participação de mulheres nos universos de computação e ciclismo, ambos marcados pela maioria absoluta de homens.
O evento acontecerá dia 02/setembro, primeiro final de semana do mês da mobilidade. Durante esta semana, criamos o evento e as artes de divulgação, abrimos as inscrições e estamos com uma lista pronta para fazer a convocação das participantes.
          `
        }
      ]
    }
  })
}

export default createStore
